import pika
import json


def send_message(presentation, status):
    print(presentation)
    dictionary = {
        "name": presentation.presenter_name,
        "email": presentation.presenter_email,
        "status": status,
        "title": presentation.title,
    }
    body = json.dumps(dictionary)
    parameters = pika.ConnectionParameters(host="rabbitmq")
    print(parameters)
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="tasks")
    channel.basic_publish(
        exchange="",
        routing_key="tasks",
        body=body,
    )
    connection.close()
