import json
import pika
from events.api_views import ConferenceListEncoder
from .models import Presentation
from django.http import JsonResponse
from events.models import Conference
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from .producer import send_message


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentations = [
            {
                "title": p.title,
                "status": p.status.name,
                "href": p.get_api_url(),
            }
            for p in Presentation.objects.filter(conference=conference_id)
        ]
        return JsonResponse({"presentations": presentations})
    elif request.method == "POST":
        content = json.loads(request.body)

        # Get the Conference object and put it in the content dict
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

    presentation = Presentation.create(**content)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
    }

    def get_extra_data(self, o):
        return {"status": o.status.name}


def api_show_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )


# pika pika stuff
def api_approve_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    presentation.approve()

    send_message(presentation, "approved")
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )


def api_reject_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    presentation.reject()

    send_message(presentation, "rejected")  # last change
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )
